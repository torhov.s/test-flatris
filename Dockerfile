#установить
FROM node

#создать каталоги скпировать туда приложение
RUN mkdir /app

#перейти в этот каталог что б yarn запустился именно из него
WORKDIR /app

#финт ушами что б при сборке каждый раз не устанавливать yarn зависимости
COPY package.json /app

#RUN запуск при сборке
RUN yarn install

COPY . /app
#RUN yarn test
RUN yarn build

#CMD запуск при docker run
CMD yarn start

#открыть порт докера
EXPOSE 3000
